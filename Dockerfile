FROM python:latest
ENV  NODE_ENV=production
RUN mkdir /code
WORKDIR /code
RUN pip install django && pip install djangorestframework
RUN pip install pygments
COPY . /code/

EXPOSE 8006
CMD ["python", "./tutorial/manage.py", "runserver", "0.0.0.0:8006"]